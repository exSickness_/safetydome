#!/usr/bin/env python3

import psycopg2
from flask import Flask, abort, request, render_template


# Connect to database
try:
	app = Flask(__name__)

	conn = psycopg2.connect(database='safetydome', user='flask', password='flask', host='localhost')
	cur = conn.cursor()
except Exception as e:
	print("Error! ",e)
	exit(1)

# Index page
@app.route('/')
def index():
	return(render_template('index.html',))


# Battle page
@app.route('/battle/<int:ID1>-<int:ID2>')
@app.route('/battle')
def battle(ID1 = None, ID2 = None):
	if ID1 is None:
		# No parameters specified
		cur.execute("SELECT * FROM fight")
		fights = cur.fetchall()
		return(render_template('battle.html', fights=fights))
	else:
		# Parameters passed in
		cur.execute("SELECT * FROM fight WHERE combatant_one=%s AND combatant_two=%s", (ID1, ID2))
		fight = cur.fetchone()
		return(render_template('battle_detail.html', fight=fight))


# Combatant page
@app.route('/combatant/<int:comID>')
@app.route('/combatant')
def combatant(comID = None):
	if comID is None:
		# No parameters specified
		cur.execute("SELECT id, name, species_id FROM combatant")
		combatants = cur.fetchall()

		# Sort list based up number of wins
		combatants.sort(key=lambda x: x[1])

		return(render_template('combatants.html', combatants=combatants))
	else:
		# Parameters passed in
		cur.execute("SELECT id, name, species_id FROM combatant WHERE id=%s", (comID,))
		combatant = cur.fetchone()
		return(render_template('combatant_detail.html', combatant=combatant))


# Results page
@app.route('/results')
def results():
	results = []

	# Retrieve total number of fighters
	cur.execute("SELECT COUNT(*) FROM combatant")
	totalCombatants = cur.fetchone()[0]

	# Loop to iterate over fighters and get their number of wins
	for i in range(1, totalCombatants+1):
		numWins = 0
		# Get the name of the fighter
		cur.execute("SELECT name FROM combatant WHERE id=%s", (i,))
		curName = cur.fetchone()[0]

		# Retrieve all fight table entries where the fighter won
		cur.execute("SELECT * FROM fight WHERE combatant_one=%s AND winner='One'", (i,))
		numWins = len(cur.fetchall())
		cur.execute("SELECT * FROM fight WHERE combatant_two=%s AND winner='Two'", (i,))
		numWins += len(cur.fetchall())
		
		# Add fighter stats to list
		results.append( [i,numWins, curName] )

	# Sort list based up number of wins
	results.sort(key=lambda x: x[1], reverse=True)

	# Loop to add rank
	for i in range(len(results)):
		results[i].append(i+1)

	return(render_template('results.html',combatants=results))



if __name__ == '__main__':
	app.run(debug=True, port=8048)
	conn.close()

